jQuery('document').ready(function() {
  $('.move-to').on('click', function(event) {
    event.preventDefault();
    
    var to = $(this).attr('href');
    
    moveTo(to);
  });
  
  var moveTo = function(section) {
   $('body,html').stop().animate({
      scrollTop: ($(section).offset().top)-61
    }, 300); 
  }
  
  $('.portfolio-item').hover(
    function() {
      $(this).find('.portfolio-info').stop().fadeIn(300);
    }, function() {
      $(this).find('.portfolio-info').stop().fadeOut(300);
    }
  );
});